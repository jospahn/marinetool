marinetool
==========

A JavaFx gui program to be used with and to test the marinelib (https://bitbucket.org/jospahn/marinelib).
Data can be read in by serial port (https://fazecast.github.io/jSerialComm/), by file or in future by ethernet UDP.
Makes use of https://github.com/HanSolo/tilesfx for the gui.