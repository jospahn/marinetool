package view;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import marinelib.LineEvent;
import marinelib.SentenceEvent;
import marinelib.listener.DataListener;
import marinelib.listener.SentenceListener;

public class SentenceTerminal extends TextFlow implements SentenceListener, DataListener {

    @Override
    public void update(SentenceEvent sentenceEvent) {

        this.getChildren().add(new Text(sentenceEvent.getLine() + "\n"));
    }

    @Override
    public void update(LineEvent lineEvent) {
        Text text = new Text(lineEvent.getLine() + "\n");
        text.setFill(Color.GRAY);
        this.getChildren().add(text);
    }
}
