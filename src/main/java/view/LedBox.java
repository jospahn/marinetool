package view;

import eu.hansolo.tilesfx.addons.Indicator;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;


public class LedBox extends GridPane
{
    private Label label = new Label();
    private Indicator indicator = new Indicator();
    private float seconds = 0;
    private float timerResetValue;

    public LedBox (String name)
    {
        label.setText(name);
        label.setPrefWidth(50);
        indicator.setDotOnColor(Color.GREEN);
        indicator.setDotOffColor(Color.DARKRED);
        indicator.setPrefSize(5, 5);
        this.addRow(0, label, indicator);
        //this.setGridLinesVisible(true);
    }

    public void setOn ()
    {
        indicator.setOn(true);
        indicator.setDotOffColor(Color.GOLDENROD);
    }

    public void setTimer (float seconds)
    {
        this.timerResetValue = seconds;
        this.seconds = seconds;
    }

    public void countDown (float step)
    {

        if (this.seconds > step)
            this.seconds -= step;
        else
            setOff();
    }

    public void resetTimer()
    {
        this.seconds = this.timerResetValue;
    }

    public void setOff ()
    {
        indicator.setOn(false);
    }

    public String getName ()
    {
        return label.getText();
    }

}
