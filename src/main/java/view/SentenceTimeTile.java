package view;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.scene.layout.StackPane;
import marinelib.SentenceEvent;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.data.TimeData;
import marinelib.data.TimeSupplier;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;

public class SentenceTimeTile extends StackPane implements SentenceListener
{
    private Tile tile;

    public SentenceTimeTile()
    {
        tile = TileBuilder.create()
                .skinType(Tile.SkinType.CLOCK)
                .title("Nmea Time")
                .prefSize(300, 300)
                .build();
        this.getChildren().add(tile);
    }

    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence =  sentenceEvent.getSentence();

        if (sentence instanceof TimeSupplier)
        {
            //PositionData position = ((PositionSupplier)sentence).getPosition();
            TimeData time = ((TimeSupplier)sentence).getTime();
            tile.setTime(System.currentTimeMillis());
        }
    }
}
