package view;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import marinelib.SentenceEvent;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.data.SpeedData;
import marinelib.data.SpeedSupplier;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;

public class SentenceSpeedGaugeTile extends Pane implements SentenceListener
{
    private Tile tile;

    public SentenceSpeedGaugeTile()
    {
        tile = TileBuilder.create()
                .skinType(Tile.SkinType.BAR_GAUGE)
                .title("Nmea Speed")
                .prefSize(300, 300)
                .build();
        this.getChildren().add(tile);
    }

    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence =  sentenceEvent.getSentence();

        if (sentence instanceof SpeedSupplier)
        {
           // SpeedData speedData = ((SpeedSupplier)sentence).getSpeedData();
           // tile.setValue(speedData.getSpeed());
        }
    }
}
