package view;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.scene.layout.StackPane;
import marinelib.SentenceEvent;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;
import marinelib.sentence.GenericSentence;
import marinelib.sentence.PositionSentence;

public class SentenceMapTile extends StackPane implements SentenceListener
{
    private Tile tile;

    public SentenceMapTile()
    {
        tile = TileBuilder.create()
                .skinType(Tile.SkinType.MAP)
                .title("Nmea Position")
                .build();
        this.getChildren().add(tile);
    }

    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence =  sentenceEvent.getSentence();

        if (sentence instanceof PositionSupplier)
        {
            PositionData position = ((PositionSupplier)sentence).getPosition();
            tile.updateLocation(position.getLatDezimal(), position.getLonDezimal());
        }
    }
}
