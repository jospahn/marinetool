package controller;

import com.fazecast.jSerialComm.SerialPort;
import javax.swing.JComboBox;
import java.io.InputStream;

/**
 *
 * Util class to easily access port names from 
 * com.fazecast.jSerialComm.SerialPort
 * And some other helpfull stuff
 * @author Jole
 */
public class SerialPortHandling {
	// Constants for Baudrates
	public static final Integer[] BAUD_RATES = {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
	public static final Integer[] NMEA_BAUD_RATES = {4800, 9600, 38400};
	public static final Integer DEFAULT_BAUD_RATE = 4800;

	private static final int READ_TIMEOUT = 100;

	private static SerialPort port;

	/**
	 * Checks available ports and returns an array of the port names
	 *
	 * @return array of port names
	 *
	 * @ToDo change as in MadBus Project (don't insert empty lines instead of cu.xx)!
	 */
	public static String[] getSystemportNames() {
		SerialPort[] ports = SerialPort.getCommPorts();
		String[] names = new String[ports.length];

		for (int i = 0; i < ports.length; i++) {
			if (!ports[i].getSystemPortName().substring(0, 2).equals("cu"))
				names[i] = ports[i].getSystemPortName();
		}

		return names;
	}

	public static boolean connect(String name, Integer baud) {
		if (port != null && port.isOpen())
			port.closePort();

		port = SerialPort.getCommPort(name);

		// If port not available, return
		if (port == null)
			return false;

		port.setBaudRate(baud);

		if (port.openPort())
		{
			port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, READ_TIMEOUT, 0);
			return true;
		}

		return false;
	}

	public static  boolean isConnected ()
	{
		return port != null && port.isOpen();
	}


	public static boolean disconnect()
	{
		if (port != null && port.isOpen())
			return port.closePort();

		return true;
	}

	public static void setBaudRate (int baud)
	{
		port.setBaudRate(baud);
		System.out.println(port.getBaudRate());
	}

	public static InputStream getInputStream ()
	{
		return port.getInputStream();
	}
}

