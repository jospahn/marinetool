package controller;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import marinelib.SentenceEvent;
import marinelib.listener.SentenceListener;
import marinelib.lwe.TransmissionGroups;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class StartWindowController implements SentenceListener
{
    @FXML private ComboBox<String> portComboBox;
    @FXML private ComboBox<Integer> baudComboBox;
    @FXML private Accordion accordion;
    @FXML private TitledPane serialPane;
    @FXML private ProgressBar comProgressBar;
    @FXML private Label comStatusLabel;
    @FXML private ToggleButton comConnectButton;
    @FXML private ComboBox<Object> transmissionGroupComboBox;
    @FXML private TextField ipTextField;
    @FXML private TextField udpPortTextField;
    @FXML private ToggleButton udpConnectButton;
    @FXML private CheckBox udpTalkerCheckBox;
    @FXML private CheckBox nifManCheckBox;
    @FXML private TextField ownIpTextField;
    @FXML private Label ownIpLabel;
    @FXML private CheckBox useLweCheckBox;
    @FXML private Label fileNameLabel;

    @FXML
    public void initialize()
    {
        accordion.expandedPaneProperty().addListener(
            (ObservableValue<? extends TitledPane> ov, TitledPane old_val, TitledPane new_val) -> {
                if (new_val == null)
                    return;

                String id = new_val.getId();

                switch (id)
                {
                    case "serialPane":
                        updatePortComboBox(SerialPortHandling.getSystemportNames());
                        updateSerialPaneStatus(SerialPortHandling.isConnected(), false);
                        break;
                    case "udpPane":
                        updateUdpPane();
                        break;
                    default:
                        break;
                }

        });


        transmissionGroupComboBox.getItems().addAll(TransmissionGroups.values());
        transmissionGroupComboBox.getItems().add(0, "manual");
        transmissionGroupComboBox.setValue(TransmissionGroups.NAVD);
        updateBaudComboBox(SerialPortHandling.NMEA_BAUD_RATES);

        accordion.setExpandedPane(serialPane);
    }

    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        Platform.runLater(() -> {
            // ToDO: this leads to the serial port showing connected state, as soon nmea data are detected. Also when they are comming from file.
            this.updateSerialPaneStatus(true, true);
            //Main.showSentenceWindow();
        });
    }

    /**
     * Handles the start button, that means leave the Start window and show the
     * main windoww.
     * @param event
     */
    @FXML protected void startButtonHandler (ActionEvent event)
    {
        Main.showSentenceWindow();
    }

    /**
     * Handles the End button: Exit the application
     * @param event
     */
    @FXML
    protected void handleEndButtonAction (ActionEvent event)
    {
        System.exit(0);
    }


    /**** Serial Pane ********************************************************/

    /**
     * Handler for Connect Button on serial pane
     * @param event
     */
    @FXML
    protected void handleComConnectButtonAction (ActionEvent event)
    {
        boolean status;
        comProgressBar.setProgress(-1.0);
        if (SerialPortHandling.isConnected())
        {
            Main.stopSerialParsing();
            status = !SerialPortHandling.disconnect();
        }
        else
        {
                status = SerialPortHandling.connect(portComboBox.getValue(), baudComboBox.getValue());
                if (status)
                   Main.setSerialSentenceInputReader(new BufferedReader(new InputStreamReader(SerialPortHandling.getInputStream())));
        }
        updateSerialPaneStatus(status, false);
    }

    /**
     * Handle baud combo box: On change write the new baudrate to the interface
     * @param event
     */
    @FXML
    protected  void handleBaudComboBoxAction (ActionEvent event)
    {
        if (SerialPortHandling.isConnected())
            SerialPortHandling.setBaudRate(baudComboBox.getValue());
    }

    /**
     * Clear and repopulate the combobox with the portnames given as String array.
     * @param ports
     */
    void updatePortComboBox(String[] ports)
    {
        // As we don't want to get an ArrayIndexOutOfBoundsexception when accessing ports[0], check length first
        if (ports.length == 0)
            return;
        portComboBox.getItems().clear();
        portComboBox.getItems().addAll(ports);
        portComboBox.setValue(ports[0]);
    }

    /**
     * Clear and repopulate the combobox with selectable baudrates, given as Integer array
     * @param baudrates
     */
    void updateBaudComboBox(Integer[] baudrates)
    {
        baudComboBox.getItems().clear();
        baudComboBox.getItems().addAll(baudrates);
        baudComboBox.setValue(4800);
    }

    void updateSerialPaneStatus (boolean connected, boolean sentencedetected)
    {
        if (connected)
        {
            if (sentencedetected)
            {
                comStatusLabel.setText("verbunden, NMEA erkannt");
                comProgressBar.setProgress(1);
            } else {
                comStatusLabel.setText("verbunden");
                comProgressBar.setProgress(0.5);
            }
            comConnectButton.setSelected(true);
            portComboBox.setDisable(true);
        } else {
            comStatusLabel.setText("nicht verbunden");
            comProgressBar.setProgress(0);
            comConnectButton.setSelected(false);
            portComboBox.setDisable(false);
        }
    }


    /**** UDP/LWE Pane *******************************************************/

    /**
     * As the change of this check box shall change then disabled status of
     * other controls, the pane must be updated.
     * @param event
     */
    @FXML
    protected void handleNifManCheckBox (ActionEvent event)
    {
        updateUdpPane();
    }

    @FXML
    protected void handleTransmissionGroupComboBox (ActionEvent event)
    {
        updateUdpPane();
    }

    @FXML
    protected void handleUdpConnectButton (ActionEvent event)
    {
        if (Main.isUdpParsing()) {
            Main.stopUdpParsing();
        } else if (Main.isUdpSending()) {
            Main.stopUdpSending();
        } else {
            try {
                InetAddress iAddr = InetAddress.getByName(ipTextField.getText());
                Integer port = new Integer(udpPortTextField.getText());
                InetAddress nif = null;
                if (nifManCheckBox.isSelected())
                {
                    nif = InetAddress.getByName(ownIpTextField.getText());
                }

                if (udpTalkerCheckBox.isSelected())
                {
                    Main.startUdpOutput(iAddr, port, nif);
                } else {
                    System.out.println(nif);
                    Main.setUdpSentenceInputReader(iAddr, port, nif);
                }
            } catch (UnknownHostException | SocketException e) {
                e.printStackTrace();
            }
        }
        updateUdpPane();
    }

    void updateUdpPane ()
    {
        /* When UDP transmissions are active, the controls shall be deactivated */
        if (Main.isUdpParsing() || Main.isUdpSending() )
        {
            udpConnectButton.setSelected(true);
            ipTextField.setDisable(true);
            udpPortTextField.setDisable(true);
            transmissionGroupComboBox.setDisable(true);
            ownIpTextField.setDisable(true);
            ownIpLabel.setDisable(true);
            nifManCheckBox.setDisable(true);

        /* otherwise they shall bve activated, some oft them depending on additional conditions  */
        } else {
            udpConnectButton.setSelected(false);
            ipTextField.setDisable(false);
            udpPortTextField.setDisable(false);
            transmissionGroupComboBox.setDisable(false);
            nifManCheckBox.setDisable(false);

            {
                transmissionGroupComboBox.setDisable(false);
                ipTextField.setDisable(true);
                udpPortTextField.setDisable(true);
            }
            {
                ipTextField.setDisable(false);
                udpPortTextField.setDisable(false);
            }

            /* The controls for manual NIF selection to be activated depending on check box */
            if (nifManCheckBox.isSelected())
            {
                ownIpTextField.setDisable(false);
                ownIpLabel.setDisable(false);
            } else {
                ownIpTextField.setDisable(true);
                ownIpLabel.setDisable(true);
            }

            ipTextField.setEditable(true);
            if (transmissionGroupComboBox.getValue() instanceof TransmissionGroups)
            {
                TransmissionGroups tg = (TransmissionGroups)transmissionGroupComboBox.getValue();
                ipTextField.setDisable(true);
                udpPortTextField.setDisable(true);
                ipTextField.setText(tg.getInetAdress().getCanonicalHostName());
                udpPortTextField.setText(tg.getPort().toString());
            }
            else
            {
                ipTextField.setDisable(false);
                udpPortTextField.setDisable(false);

            }

            /*
            nifComboBox.getItems().clear();
            try {
                Enumeration<NetworkInterface> nifs = NetworkInterface.getNetworkInterfaces();
                while (nifs.hasMoreElements()) {
                    nifComboBox.getItems().add(nifs.nextElement().getName());
                }
                String nifName = Main.getNifName();
                if (nifName != null)
                {
                    nifComboBox.setValue(nifName);
                }
            } catch (SocketException e) {
                e.printStackTrace();
            }
            */
        }
    }


    /**** UDP/LWE Pane *******************************************************/

    @FXML
    protected void handleFileOpenButtonAction (ActionEvent event)
    {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        if (file == null)
            return;
        fileNameLabel.setText(file.getName());
        try {
            Main.setFileSentenceInputReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
