package controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextFlow;
import marinelib.LineEvent;
import marinelib.SentenceEvent;
import marinelib.listener.DataListener;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;
import marinelib.sentence.SentenceTypesEnum;
import view.LedBox;
import view.SentenceTerminal;

import java.util.ArrayList;


public class SentenceWindow implements SentenceListener, DataListener, Runnable
{
    @FXML private Button backButton;
    @FXML private Button mapButton;
    @FXML private FlowPane indicatorFlowPane;
    @FXML private ScrollPane terminalPane;

    ArrayList<LedBox> ledBoxArrayList = new ArrayList<>();
    SentenceTerminal sentenceTerminal = new SentenceTerminal();

    //todo: make time adjustable by control
    private static final float timeout = 5;

    @FXML public void initialize()
    {
        indicatorFlowPane.setHgap(50);
        indicatorFlowPane.setVgap(10);
        for (SentenceTypesEnum ste : SentenceTypesEnum.values())
        {
            LedBox ledBox = new LedBox(ste.name());
            ledBox.setTimer(timeout);
            ledBoxArrayList.add(ledBox);
            indicatorFlowPane.getChildren().add(ledBox);
        }

        terminalPane.setContent(sentenceTerminal);
        terminalPane.vvalueProperty().bind(sentenceTerminal.heightProperty());
        new Thread(this, "ReceivedTimer").start();
    }

    @FXML protected void handleBackButton (ActionEvent event)
    {
        Main.showStartWindow();
    }

    @FXML protected void handleMapButton (ActionEvent event)
    {
        Main.showDashBoard();
        //Main.showMapTile();
    }

    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence = sentenceEvent.getSentence();
        for (LedBox lb : ledBoxArrayList)
        {
            if (lb.getName().equals(sentence.getSid()))
            {
                lb.setOn();
                lb.resetTimer();
            }
        }
        Platform.runLater(() -> sentenceTerminal.update(sentenceEvent));
    }

    @Override
    public void update(LineEvent lineEvent) {
        Platform.runLater(() -> sentenceTerminal.update(lineEvent));
    }

    @Override
    public void run()
    {
        int cntSecond = 10;
        while (true)
        {
            try
            {
                Thread.sleep(100);
            } catch (InterruptedException ex)
            {
            }

            for (LedBox lb : ledBoxArrayList)
            {
                lb.countDown(0.1f);
            }

            if (--cntSecond == 0)
            {
                cntSecond = 10;
                Main.triggerLineRead();
            }

        }


    }
}
