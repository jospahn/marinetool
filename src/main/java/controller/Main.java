package controller;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import marinelib.SentenceFileInputReader;
import marinelib.SentenceInputReader;
import marinelib.SentenceParser;
import marinelib.SerialSentenceInputReader;
import marinelib.listener.LineListener;
import marinelib.udp.UdpOutputQueue;
import marinelib.udp.UdpSentenceInputReader;
import view.SentenceMapTile;

import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

public class Main extends Application
{
    private static SerialSentenceInputReader serialSentenceInputReader;
    private static SentenceParser sentenceParser = new SentenceParser();

    private static NetworkInterface nif;
    private static SentenceInputReader sentenceInputReader;
    private static FileReader fileReader;
    private static UdpSentenceInputReader udpSentenceInputReader;
    private static UdpOutputQueue udpOutputQueue;


    private static StartWindowController startWindowController;
    private static SentenceWindow sentenceWindow;
    private static DashWindow dashWindow;


    private static Stage primaryStage;
    private static Stage dashBoardStage;
    private static Scene startScene;
    private static Scene sentenceScene;

    public static void main(String[] args)
    {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception
    {
        Main.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
        Parent root = loader.load();
        //startStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("Nmea Tool");
        startScene = new Scene(root, 600, 400);
        primaryStage.setScene(startScene);

        startWindowController = loader.getController();
        primaryStage.show();

        loader = new FXMLLoader(Main.class.getResource("/fxml/sentenceWindow.fxml"));
        root = loader.load();
        sentenceWindow = loader.getController();
        sentenceScene = (new Scene(root, 600, 400));


        loader = new FXMLLoader(Main.class.getResource("/fxml/dashWindow.fxml"));
        root = loader.load();
        dashWindow = loader.getController();
        dashBoardStage = new Stage();
        dashBoardStage.setScene(new Scene(root, 600, 600));


        sentenceParser.addSentenceListener(startWindowController, null);
        sentenceParser.addSentenceListener(sentenceWindow, null);
        sentenceParser.addSentenceListener(dashWindow, null);
        sentenceParser.addDataListener(sentenceWindow);

        System.setProperty("java.net.preferIPv4Stack", "true");


        primaryStage.setOnHiding( event -> {
            //ToDo: add things to be done when closing primary stage
            try {
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    static public void setFileSentenceInputReader (File f) throws IllegalArgumentException, FileNotFoundException {
        if (!f.isFile() || !f.canRead())
            throw new IllegalArgumentException("Not a readable file");

        if (fileReader != null )
        {
            try {
                fileReader.close();
            } catch (IOException e) {
            }
        }

        fileReader = new FileReader(f);

        if (sentenceInputReader != null)
            sentenceInputReader.stop();

        sentenceInputReader = new SentenceFileInputReader(new BufferedReader(fileReader));

        sentenceInputReader.addListener(LineListener.class, sentenceParser);
        //nmeaInputReader.start();
    }

    static public void setSerialSentenceInputReader (BufferedReader reader)
    {
        serialSentenceInputReader = new SerialSentenceInputReader(reader);
        serialSentenceInputReader.addListener(LineListener.class, sentenceParser);
        serialSentenceInputReader.start();
    }

    static public boolean setUdpSentenceInputReader (InetAddress iAddr, int port, InetAddress nifAddr) {
        if (nifAddr == null)
        {
            udpSentenceInputReader = new UdpSentenceInputReader(iAddr, port);
        }
        else
        {
            udpSentenceInputReader = new UdpSentenceInputReader(iAddr, port, nifAddr);
        }

        udpSentenceInputReader.addListener(LineListener.class, sentenceParser);
        udpSentenceInputReader.start();
        return true;        // ToDO: Errorhandling !!!
    }

    static public boolean isUdpParsing()
    {
        if (udpSentenceInputReader == null)
            return false;

        return udpSentenceInputReader.isStarted();
    }

    static public String getNifName()
    {
        if (nif == null)
            return null;
        return nif.getName();
    }

    static public void startUdpOutput(InetAddress iAddr, int port, InetAddress nif)
    {
        try {
            if (nif==null)
                udpOutputQueue = new UdpOutputQueue(iAddr, port);
            else
                udpOutputQueue = new UdpOutputQueue(iAddr, port, nif);
            udpOutputQueue.start();
            if (sentenceParser != null)
                sentenceParser.addSentenceListener(udpOutputQueue, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static public boolean isUdpSending()
    {
        if (udpOutputQueue == null)
            return false;

        return udpOutputQueue.isStarted();
    }

    static public void stopSerialParsing()
    {
        serialSentenceInputReader.stop();
    }

    static public void stopUdpParsing()
    {
        udpSentenceInputReader.stop();
    }

    static public void stopUdpSending()
    {
        udpOutputQueue.stop();
    }

    static public void showSentenceWindow() {
        primaryStage.setScene(sentenceScene);
    }

    static public void showStartWindow()
    {
        primaryStage.setScene(startScene);
    }

    static public void showDashBoard()
    {
        dashBoardStage.show();
    }

    static public void triggerLineRead()
    {
        if (sentenceInputReader != null)
            sentenceInputReader.readLine();
    }

}

