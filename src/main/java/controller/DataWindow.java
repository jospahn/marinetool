package controller;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.tools.FlowGridPane;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import marinelib.SentenceEvent;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.data.TimeData;
import marinelib.data.TimeSupplier;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class DataWindow implements SentenceListener
{

    @FXML TextField textPosition;
    @FXML TextField textFixtime;
    @FXML TextField textSOG;
    @FXML TextField textCOGM;
    @FXML TextField textCOGT;

    @FXML public void initialize()
    {
    }


    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence =  sentenceEvent.getSentence();

        if (sentence instanceof PositionSupplier)
        {
            textPosition.setText(((PositionSupplier) sentence).getPosition().toString());
        }

        if (sentence instanceof TimeSupplier)
        {
            textFixtime.setText(((TimeSupplier) sentence).getTime().toString());
        }
//        Platform.runLater(() -> sentenceTerminal.update(sentenceEvent));
    }
}
