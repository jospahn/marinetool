package controller;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.tools.FlowGridPane;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import marinelib.SentenceEvent;
import marinelib.data.PositionData;
import marinelib.data.PositionSupplier;
import marinelib.data.TimeData;
import marinelib.data.TimeSupplier;
import marinelib.listener.SentenceListener;
import marinelib.sentence.AbstractSentence;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class DashWindow implements SentenceListener
{
    @FXML private AnchorPane anchorPane;
  //  @FXML private GridPane gridPane;

    private static final double TILE_SIZE = 150;

    private Tile mapTile;
    private Tile coordinatesTile;
    private Tile clockTile;


    @FXML public void initialize()
    {
        //gridPane.prefWidthProperty().bind(anchorPane.widthProperty());
        //gridPane.prefHeightProperty().bind(anchorPane.heightProperty());


        mapTile = TileBuilder.create()
                .prefSize(TILE_SIZE, TILE_SIZE)
                .skinType(Tile.SkinType.MAP)
                .title("Nmea Position")
                .mapProvider(Tile.MapProvider.STREET)
                .build();


        clockTile = TileBuilder.create()
                .prefSize(TILE_SIZE, TILE_SIZE)
                .skinType(Tile.SkinType.CLOCK)
                .title("Time Of Fix")
                .dateVisible(false)
                .secondsVisible(true)
                .locale(Locale.GERMAN)
                .build();

        coordinatesTile = TileBuilder.create()
                .prefSize(TILE_SIZE, TILE_SIZE)
                .skinType(Tile.SkinType.TEXT)
                .title("Position")
                .build();


        FlowGridPane flowGridPane = new FlowGridPane(2, 2, mapTile, clockTile, coordinatesTile);
        flowGridPane.prefWidthProperty().bind(anchorPane.widthProperty());
        flowGridPane.prefHeightProperty().bind(anchorPane.heightProperty());
        flowGridPane.setHgap(5);
        flowGridPane.setVgap(5);
        flowGridPane.setPadding(new Insets(5));
        flowGridPane.setBackground(new Background(new BackgroundFill(Tile.BACKGROUND.darker(), CornerRadii.EMPTY, Insets.EMPTY)));
        anchorPane.getChildren().add(flowGridPane);
    }


    @Override
    public void update(SentenceEvent sentenceEvent)
    {
        AbstractSentence sentence =  sentenceEvent.getSentence();

        if (sentence instanceof PositionSupplier)
        {
            PositionData position = ((PositionSupplier)sentence).getPosition();
            mapTile.updateLocation(position.getLatDezimal(), position.getLonDezimal());
            coordinatesTile.setDescription(position.latString() + "\n" + position.lonString());
            mapTile.setText(sentence.getTid() + sentence.getSid());
            coordinatesTile.setText(sentence.getTid() + sentence.getSid());
        }

        if (sentence instanceof TimeSupplier)
        {
            TimeData time = ((TimeSupplier)sentence).getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
            long epochSeconds = 0;
            try {
                epochSeconds = sdf.parse("" + time.getHours() + time.getMinutes() + time.getSeconds() ).getTime()/ 1000;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            clockTile.setTime(epochSeconds);
            clockTile.setText(sentence.getTid() + sentence.getSid());

        }
//        Platform.runLater(() -> sentenceTerminal.update(sentenceEvent));
    }
}
